const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const app = express();

const port = process.env.port || 3000;


app.listen(port, () => {
    console.log(`Aplicação rodando na porta ${port}`)
});